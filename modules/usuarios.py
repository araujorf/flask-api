from db.db_conn import Database
from flask import jsonify
from utils import hash
from config import settings

conn_string = settings.db_location
print(conn_string)

def get_all():
    db = Database(conn_string)
    sql = "SELECT * FROM usuarios"
    usuarios = db.query(sql)
    return jsonify({"data": usuarios})


def get_one(username):
    db = Database(conn_string)
    sql = "SELECT * FROM usuarios WHERE usuario=?"
    usuario = db.query_one(sql, [username])
    return jsonify({"data": usuario}) if usuario else ('No se encontro', 404)


def create(usuario):
    db = Database(conn_string)
    data = [
        usuario.get("usuario"),
        hash.sha2_string(usuario.get("password")),
        usuario.get("descripcion"),
        usuario.get("email"),
        usuario.get("nivel")
    ]
    # Chequeo usuario existente
    if usuario_existe(data[0]):
        return 'Ya existe', 409
    else:
        sql = """INSERT INTO usuarios (usuario, password, descripcion, email, nivel)
        VALUES(?, ?, ?, ?, ?)"""
        db.execute(sql, data)
        return 'OK', 201

# Esta operacion no va a funcionar porque sqlite solo acepta arrays y no diccionarios
# Pero esta hecha de manera que dinamicamente actualiza los campos que se envien. En mysql si funcionaria
# Se deja solo a modo de ejemplo
def update(username, usuario):
    db = Database(conn_string)
    data = {
        "descripcion": usuario.get("descripcion"),
        "email": usuario.get("email"),
        "nivel": usuario.get("nivel"),
        "password": usuario.get("password"),
        "habilitado": usuario.get("habilitado")
    }

    cleaned_data = {k:v for k,v in data.items() if v is not None}
    # TODO Check if this can be shortened or using f strings
    sql = 'UPDATE usuarios SET {} WHERE usuario = '.format(', '.join('{}=%({})s'.format(k,k) for k in cleaned_data))+'"'+username + '"'
    db.execute(sql, data)


def delete(username):
    db = Database(conn_string)
    if (usuario_existe(username)):
        sql = "DELETE FROM usuarios WHERE usuario=?"
        db.execute(sql, [username])
        return 'OK', 200
    else:
        return 'No se encontro nombre de usuario', 404


# Funciones auxiliares
def usuario_existe(usuario):
    db = Database(conn_string)
    sql = "SELECT COUNT(*) FROM usuarios AS cuenta WHERE usuario=?"
    usuario_count =  db.query_one(sql, [usuario])
    return usuario_count[0] > 0
